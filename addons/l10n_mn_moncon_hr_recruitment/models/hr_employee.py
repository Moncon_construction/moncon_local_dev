# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import api, fields, models

class HrEmployee (models.Model):
    _inherit = "hr.employee"
    engagement_in_company = fields.Date(string='Date of Engagement in company', store=True, required=True, default=False)
