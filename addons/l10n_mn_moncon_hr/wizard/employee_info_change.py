# -*- coding: utf-8 -*-
from odoo import api, fields, models


class EmployeeInfoChange(models.TransientModel):
    _inherit = 'employee.info.change'
    _description = 'Wizard for employee info change'

    department_id = fields.Many2one('hr.department', string='Хэлтэс')
    job_id = fields.Many2one('hr.job', string='Албан тушаал')
    hr_employee_id = fields.Many2one('hr.employee', u'ХН ажилтан')

    @api.multi
    def employee_info_change(self):
        super(EmployeeInfoChange, self).employee_info_change()

        context = dict(self._context or {})
        employee_ids = self.env['hr.employee'].browse(context.get('active_ids'))
        for employee in employee_ids:
            if self.department_id:
                self.env.cr.execute(
                    "UPDATE hr_employee SET department_id = '%s' WHERE id = %s" % (self.department_id.id, employee.id))
            if self.job_id:
                self.env.cr.execute(
                    "UPDATE hr_employee SET job_id = '%s' WHERE id = %s" % (self.job_id.id, employee.id))
            if self.hr_employee_id:
                self.env.cr.execute(
                    "UPDATE hr_employee SET hr_employee_id = '%s' WHERE id = %s" % (self.hr_employee_id.id, employee.id))



