# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError
import pytz
from odoo.http import request
from datetime import date, datetime, timedelta
from calendar import monthrange
from odoo.addons.l10n_mn_web.models.time_helper import *


class HrAttendanceRepair(models.Model):
    _inherit = 'hr.attendance.repair'
    _order = 'date_of_attendance ASC'

    STATE_SELECTION = [
        ('draft', 'New'),
        ('send', 'Sent request'),
        ('approved', 'Approved'),
        ('canceled', 'Request canceled'),
        ('accepted', 'Registered'),
    ]

    type_of_attendance = fields.Selection([('in', u'Орсон'), ('out', u'Гарсан'), ('in_out', u'Бүтэн өдөр')], u'Ирцийн төрөл', default='in', required=True)
    date_of_attendance = fields.Datetime(u'Ирц бүртгүүлэх огноо', default=fields.Datetime.now, required=True)
    out_date_of_attendance = fields.Datetime(u'Гарсан цаг')
    reason_of_attendance = fields.Many2one('hr.attendance.repair.reason', 'Reason', required=True)
    comment = fields.Text('Comment', required=True)
    check_in_attendance_tmp = fields.Char('Check in attendance', compute='_compute_attendance')
    check_out_attendance_tmp = fields.Char('Check out attendance', compute='_compute_attendance')
    check_in_attendance = fields.Char('Check in attendance')
    check_out_attendance = fields.Char('Check out attendance')
    cancel_comment = fields.Char('Цуцласан тайлбар', readonly=True)
    state = fields.Selection(STATE_SELECTION, default='draft', help="Status of the attendance repair", track_visibility='onchange')
    hr_employee_id = fields.Many2one(related='employee_id.hr_employee_id', store=True, readonly=False, string='ХН ажилтан')

    @api.multi
    def _compute_attendance(self):
        for obj in self:
            if obj.date_of_attendance:
                tz = get_user_timezone(self.env.user)
                date_time_obj = pytz.utc.localize(
                    datetime.strptime(obj.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                dt = str(date_time_obj.date())
                attendance = self.env['hr.attendance'].search([('employee_id', '=', obj.employee_id.id)])
                if attendance:
                    check_in = False
                    check_out = False
                    for at in attendance:
                        if at.check_in:
                            check_in_date = str(
                                pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(
                                    tz).date())
                            if check_in_date == dt:
                                check_in = str(
                                    pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(
                                        tz).strftime('%Y-%m-%d %H:%M:%S'))
                        if at.check_out:
                            check_out_date = str(
                                pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(
                                    tz).date())
                            if check_out_date == dt:
                                check_out = str(
                                    pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(
                                        tz).strftime('%Y-%m-%d %H:%M:%S'))
                    obj.check_in_attendance_tmp = str(check_in)
                    if check_in:
                        obj.check_in_attendance_tmp = str(check_in)
                    else:
                        obj.check_in_attendance_tmp = _('No attendance')

                    if check_out:
                        obj.check_out_attendance_tmp = str(check_out)
                    else:
                        obj.check_out_attendance_tmp = _('No attendance')

    @api.model
    def create(self, vals):
        creation = super(HrAttendanceRepair, self).create(vals)
        datetime_object = datetime.strptime(vals['date_of_attendance'], '%Y-%m-%d %H:%M:%S')
        firstmonth = '%s-%s-01' % (datetime_object.year, datetime_object.month)
        month = monthrange(datetime_object.year, datetime_object.month)
        lastmonth = '%s-%s-%s' % (datetime_object.year, datetime_object.month, month[1])
        repair_ids = self.env['hr.attendance.repair'].search(
            [('employee_id', '=', vals['employee_id']), ('date_of_attendance', '>=', firstmonth),
             ('date_of_attendance', '<=', lastmonth), ('reason_of_attendance.type', '=', 'no_finger')])
        if len(repair_ids) > 2:
            raise UserError(u'Та хуруу мартсан хүсэлт тухайн сард 2 үүсгэх боломжтой')
        detail = self.env['hr.attendance.detail'].search([('report_datetime', '=', creation.date_of_attendance)])
        if not detail:
            self.env['hr.attendance.detail'].create({'report_datetime': creation.date_of_attendance, 'name': 'Repair Attendance'})
        return creation

    @api.onchange('reason_of_attendance')
    def onchange_reason_of_attendance(self):
        for obj in self:
            datetime_object = datetime.strptime(obj.date_of_attendance, '%Y-%m-%d %H:%M:%S')
            firstmonth = '%s-%s-01' % (datetime_object.year, datetime_object.month)
            month = monthrange(datetime_object.year, datetime_object.month)
            lastmonth = '%s-%s-%s' % (datetime_object.year, datetime_object.month, month[1])
            repair_ids = self.env['hr.attendance.repair'].search(
                [('employee_id', '=', obj.employee_id.id), ('date_of_attendance', '>=', firstmonth),
                 ('date_of_attendance', '<=', lastmonth), ('reason_of_attendance.type', '=', 'no_finger')])
            if len(repair_ids) > 2:
                raise UserError(u'Та хуруу мартсан хүсэлт тухайн сард 2 үүсгэх боломжтой')

    @api.onchange('date_of_attendance')
    def _onchange_date_of_attendance(self):
        if self.date_of_attendance:
            tz = get_user_timezone(self.env.user)
            date_time_obj = pytz.utc.localize(
                datetime.strptime(self.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            dt = str(date_time_obj.date())
            attendance = self.env['hr.attendance'].search([('employee_id', '=', self.employee_id.id)])
            if attendance:
                check_in = False
                check_out = False
                for at in attendance:
                    if at.check_in:
                        check_in_date = str(
                            pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(
                                tz).date())
                        if check_in_date == dt:
                            check_in = str(
                                pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(
                                    tz).strftime('%Y-%m-%d %H:%M:%S'))
                    if at.check_out:
                        check_out_date = str(
                            pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(
                                tz).date())
                        if check_out_date == dt:
                            check_out = str(
                                pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(
                                    tz).strftime('%Y-%m-%d %H:%M:%S'))
                self.check_in_attendance_tmp = self.check_in_attendance = str(check_in)
                if check_in:
                    self.check_in_attendance_tmp = self.check_in_attendance = str(check_in)
                else:
                    self.check_in_attendance_tmp = self.check_in_attendance = _('No attendance')

                if check_out:
                    self.check_out_attendance_tmp = self.check_out_attendance = str(check_out)
                else:
                    self.check_out_attendance_tmp = self.check_out_attendance = _('No attendance')

    @api.onchange('out_date_of_attendance')
    def _onchange_out_date_of_attendance(self):
        if self.type_of_attendance == 'in_out':
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(self.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            date_to = pytz.utc.localize(datetime.strptime(self.out_date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)

            if date_from.date() != date_to.date():
                raise UserError(u'Та он сар өдрөө зөв сонгоно уу')

    @api.multi
    def cancel(self):
        for obj in self:
            if obj.type_of_attendance == 'in':
                attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', obj.employee_id.id), ('check_in', '=', obj.date_of_attendance)])
                for attendance_id in attendance_ids.filtered(lambda x: x.check_out):
                    attendance_id.write({'check_in': datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=-1)})
                attendance_ids.filtered(lambda x: not x.check_out).unlink()
            else:
                attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', obj.employee_id.id), ('check_out', '=', obj.date_of_attendance)])
                for attendance_id in attendance_ids.filtered(lambda x: x.check_in):
                    if attendance_id.check_in:
                        attendance_id.write({'check_out': datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)})
                attendance_ids.filtered(lambda x: not x.check_in).unlink()
            obj.write({'state': 'canceled'})

    @api.multi
    def send(self):
        for object in self:
            if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
                tz = get_user_timezone(self.env.user)
                date_from = pytz.utc.localize(datetime.strptime(object.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
                today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                if today.weekday() == 0:
                    str_start = today - timedelta(days=7)
                else:
                    str_start = today - timedelta(today.weekday())
                if str_start.date() > date_from.date():
                    raise UserError(u'Тухайн долоо хоногийн ирц нөхөн засах хүсэлтийн хугацаа хэтэрсэн байна!!!')

                if object.workflow_id:
                    workflow_obj = self.env['workflow.config']
                    success, current_sequence = workflow_obj.send('workflow.history', 'attendance_repair_id', object, self.create_uid.id)
                    is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(object.workflow_id.id, object, self.env.user.id, object.check_sequence + 1,
                                                                                                     'next')
                    if is_next:
                        object.check_users = [(6, 0, next_user_ids)]
                    if success:
                        object.check_sequence = current_sequence
                        object.ensure_one()
                        self.write({'state': 'send'})
                else:
                    raise UserError(_("You Don't configure a Workflow for Attendance Repair!"))
            else:
                if object.workflow_id:
                    workflow_obj = self.env['workflow.config']
                    success, current_sequence = workflow_obj.send('workflow.history', 'attendance_repair_id', object, self.create_uid.id)
                    is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(object.workflow_id.id, object, self.env.user.id,
                                                                                                     object.check_sequence + 1, 'next')
                    if is_next:
                        object.check_users = [(6, 0, next_user_ids)]
                    if success:
                        object.check_sequence = current_sequence
                        object.ensure_one()
                        self.write({'state': 'send'})

    @api.multi
    def approve(self):
        for object in self:
            if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
                object.write({'state': 'approved'})
                tz = get_user_timezone(self.env.user)
                date_from = pytz.utc.localize(datetime.strptime(object.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
                today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                if today.weekday() == 0:
                    str_start = today - timedelta(days=7)
                else:
                    str_start = today - timedelta(today.weekday())
                if str_start.date() > date_from.date():
                    if today.weekday() == 1:
                        if today.hour >= 18:
                            raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                        else:
                            str_start = today - timedelta(days=8)
                            if str_start.date() > date_from.date():
                                raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                    else:
                        raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
            else:
                object.write({'state': 'approved'})

    @api.multi
    def unlink(self):
        for repair in self:
            if repair.state != 'draft':
                raise UserError(u'Ноорог төлөвтөй устгана!!!')
        return super(HrAttendanceRepair, self).unlink()

    @api.multi
    def accept(self):
        for obj in self:
            check = False
            attend_id = ''
            attendance = obj.env['hr.attendance'].search([('employee_id', '=', obj.employee_id.id)])
            if attendance:
                for att in attendance:
                    # Шинэ зүйл эхлэх гэж байна
                    get_date_in = get_day_like_display(att.check_in, self.env.user).date() if att.check_in else ''
                    get_date_out = get_day_like_display(att.check_out, self.env.user).date() if att.check_out else ''
                    fix_date = get_day_like_display(obj.date_of_attendance, self.env.user).date()
                    if get_date_in == fix_date or get_date_out == fix_date:
                        check = True
                        attend_id = att
            if check:
                if obj.type_of_attendance == 'in':
                    attend_id.write({'check_in': obj.date_of_attendance})
                    obj.write({'state': 'accepted'})
                elif obj.type_of_attendance == 'in_out':
                    attend_id.write({'check_in': obj.date_of_attendance})
                    attend_id.write({'check_out': obj.out_date_of_attendance})
                    obj.write({'state': 'accepted'})
                else:
                    attend_id.write({'check_out': obj.date_of_attendance})
                    obj.write({'state': 'accepted'})
            else:
                if obj.type_of_attendance == 'in':
                    self.env['hr.attendance'].create({'employee_id': obj.employee_id.id, 'check_in': obj.date_of_attendance, 'is_attendance_repair': True})
                    obj.write({'state': 'accepted'})
                elif obj.type_of_attendance == 'in_out':
                    self.env['hr.attendance'].create(
                        {'employee_id': obj.employee_id.id, 'check_in': obj.date_of_attendance, 'check_out': obj.out_date_of_attendance, 'is_attendance_repair': True})
                    obj.write({'state': 'accepted'})
                else:
                    self.env['hr.attendance'].create({'employee_id': obj.employee_id.id, 'check_out': obj.date_of_attendance, 'is_attendance_repair': True})
                    obj.write({'state': 'accepted'})
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('workflow.history', 'attendance_repair_id', self, self.env.user.id)


class HrAttendanceRepairReason(models.Model):
    _inherit = 'hr.attendance.repair.reason'

    type = fields.Selection([('no_finger', u'Хуруу мартсан'),
                             ('business_trip', u'Томилолт'),
                             ('other', u'Бусад')], string=u'Төрөл')
