# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class HrAttendanceConfirm(models.TransientModel):

    _name = 'hr.attendance.confirm'

    @api.multi
    def confirm(self):
        context = dict(self._context or {})
        repair_ids = self.env['hr.attendance.repair'].browse(context.get('active_ids'))
        for repair_id in repair_ids:
            repair_id.accept()