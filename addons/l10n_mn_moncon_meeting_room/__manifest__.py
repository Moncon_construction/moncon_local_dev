# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - meeting room',
    'version': '1.0',
    'author': 'Moncon LLC',
    "description": """
     Уг модуль нь Монконы Хурлын өрөөний модуль болно.
 """,
    'website': 'http://www.moncon.erp.mn',
    'images': [''],
    'depends': ['l10n_mn_base'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/moncon_meeting_room_view.xml'
    ],
    'installable': True,
    'auto_install': False
}
