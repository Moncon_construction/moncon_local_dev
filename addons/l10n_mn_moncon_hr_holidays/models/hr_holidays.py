# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo import tools

import pytz


from datetime import timedelta
from odoo.exceptions import UserError, AccessError, ValidationError
from openerp.tools import float_compare

class HrHolidays(models.Model):

    _inherit = "hr.holidays"

    state = fields.Selection([
        ('draft', u'Шинэ'),
        ('confirm', u'Хүсэлт илгээгдсэн'),
        ('validate', 'Approved'),
        ('refuse', u'Хүсэлт цуцлагдсан'),
        ('accepted', u'Бүртгэгдсэн')
    ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='draft',
        help="The status is set to 'To Submit', when a holiday request is created." +
             "\nThe status is 'To Approve', when holiday request is confirmed by user." +
             "\nThe status is 'Refused', when holiday request is refused by manager." +
             "\nThe status is 'Approved', when holiday request is approved by manager.")

    hr_employee_id = fields.Many2one(related='employee_id.hr_employee_id', store=True, readonly=False, string='ХН ажилтан')
    cancelled_reason = fields.Char('Цуцалсан тайлбар', readonly=True)

    @api.multi
    def _track_subtype(self, init_values):
        return False

    @api.multi
    def action_send(self):
        if self.type == 'remove' and not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            if today.weekday() == 0:
                str_start = today - timedelta(days=7)
            else:
                str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date():
                raise UserError(u'Тухайн долоо хонгын амралт чөлөөны хугацааны хязгаар хэтэрсэн байна!!!')
        return super(HrHolidays, self).action_send()

    @api.multi
    def action_approve(self):
        if self.type == 'remove' and not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_start = today - timedelta(today.weekday())
            if today.weekday() == 2:
                str_start = today - timedelta(8)
            else:
                str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date():
                if today.weekday() == 1:
                    if today.hour >=18:
                        raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                    else:
                        str_start = today - timedelta(days=8)
                        if str_start.date() > date_from.date():
                            raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                else:
                    raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
        return super(HrHolidays, self).action_approve()

    @api.multi
    def register(self):
        for obj in self:
            obj.state = 'accepted'

    nohon_holidays_type = fields.Selection([('nohon_add', u'Нөхөн амрах хоног хуримтлуулах'),
                                            ('nohon_remove', u'Нөхөн амрах хоног биеэр эдлэх')], default='nohon_add')
    add_date_from = fields.Datetime('Date from')
    add_date_to = fields.Datetime('Date To')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')

    @api.multi
    @api.onchange('add_date_from')
    def _compute_check_in(self):
        for obj in self:
            if obj.add_date_from:
                start = datetime.strptime(obj.add_date_from, '%Y-%m-%d %H:%M:%S').date()
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(obj.employee_id, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.check_out = attendance.check_out

    # @api.model
    # def create(self, vals):
    #     res = super(HrHolidays, self).create(vals)
    #     detail = self.env['hr.attendance.detail'].search([('report_datetime', '=', res.date_from)])
    #     if not detail and res.date_from:
    #         self.env['hr.attendance.detail'].create({'report_datetime': res.date_from, 'name': 'Holidays'})
    #
    #     detailtype = self.env['hr.attendance.detail'].search([('report_datetime', '=', res.add_date_from)])
    #     if not detailtype and res.add_date_from:
    #         self.env['hr.attendance.detail'].create({'report_datetime': res.add_date_from, 'name': 'Holidays'})
    #     return res

    @api.onchange('nohon_holidays_type')
    def _onchange_nohon_holidays_type(self):
        config_hr_holidays = self.env['hr.holidays.status'].search([('is_nohon_amrakh', '=', True)], limit=1)
        for obj in self:
            obj.holiday_status_id = config_hr_holidays.id
            if self.nohon_holidays_type == 'nohon_add':
                obj.type = 'add'
                obj.date_from = False
                obj.date_to = False
            else:
                obj.add_date_from = False
                obj.add_date_to = False
                obj.type = 'remove'

    @api.onchange('add_date_from')
    def _onchange_add_date_from(self):
        date_from = self.add_date_from
        date_to = self.add_date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.nohon_holidays_type == 'nohon_add':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from, self.env.user).hour <= 12 and get_day_like_display(date_to, self.env.user).hour >= 15 and get_day_like_display(date_to, self.env.user).hour <= 21:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 - 1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.nohon_holidays_type == 'nohon_add':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8

    @api.onchange('number_of_hours_temp')
    def _onchange_number_of_hours_temp(self):
        super(HrHolidays, self)._onchange_number_of_hours_temp()
        if self.nohon_holidays_type == 'nohon_add':
            self.number_of_days_temp = self.number_of_hours_temp / 8

    @api.onchange('add_date_to')
    def _onchange_add_to_date_from(self):
        date_from = self.add_date_from
        date_to = self.add_date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.nohon_holidays_type == 'nohon_add':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from, self.env.user).hour <= 12 and get_day_like_display(date_to, self.env.user).hour >= 15 and get_day_like_display(date_to, self.env.user).hour <= 20:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 -1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.nohon_holidays_type == 'nohon_add':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8

