# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError
from odoo import exceptions
from odoo import tools
from datetime import timedelta, datetime
import pytz

class MonconHrHolidays(models.Model):
    _name = "moncon.hr.holidays"
    _inherit = 'mail.thread'

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    name = fields.Char(u'Нэр')
    type = fields.Selection([('add', u'Нөхөн амрах хоног хуримтлуулах'),
                             ('remove', u'Нөхөн амрах хоног биеэр эдлэх')], default='add')
    date_from = fields.Datetime(u'Эхлэх өдөр')
    date_to = fields.Datetime(u'Дуусах өдөр')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Хүсэлт илгээгдсэн'),
                              ('approved', u'Зөвшөөрсөн'),
                              ('refuse', u'Хүсэлт цуцлагдсан'),
                              ('accepted', u'Бүртгэгдсэн')
                              ], 'Төлөв', default='draft', track_visibility='onchange')
    comment = fields.Char(u'Тайлбар')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')
    employee_id = fields.Many2one('hr.employee', string='Ажилтан', index=True, readonly=True,
                                  states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]}, default=_default_employee)
    department_id = fields.Many2one('hr.department', related='employee_id.department_id', string='Хэлтэс', readonly=True, store=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_workflow_history = fields.Boolean(compute='_is_workflow_history')
    number_of_hours_temp = fields.Float('Allocation', readonly=True, copy=False,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    number_of_days_temp = fields.Float('Allocation', readonly=True, copy=False,
                                       states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    notes = fields.Text('Notes')
    history_lines = fields.One2many('moncon.hr.holidays.workflow.history', 'history', 'Workflow History')
    cancelled_reason = fields.Char('Цуцалсан тайлбар', readonly=True)


    @api.constrains('date_from', 'date_to')
    def _check_date(self):
        for holiday in self:
            domain = [
                ('date_from', '<=', holiday.date_to),
                ('date_to', '>=', holiday.date_from),
                ('employee_id', '=', holiday.employee_id.id),
                ('id', '!=', holiday.id),
                ('type', '=', holiday.type),
                ('state', '!=', 'refuse'),
            ]
            print(domain, 'ddd\n\n\n')
            nholidays = self.search_count(domain)
            if nholidays:
                raise ValidationError(_('You can not have 2 leaves that overlaps on same day!'))

    @api.onchange('date_from')
    def _onchange_add_date_from(self):
        date_from = self.date_from
        date_to = self.date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.type == 'add':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from,
                                                                                                     self.env.user).hour <= 12 and get_day_like_display(date_to,
                                                                                                                                                        self.env.user).hour >= 15 and get_day_like_display(
                        date_to, self.env.user).hour <= 21:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 - 1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.type == 'add':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8
            else:
                if (date_to and date_from) and (date_from <= date_to):
                    self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
                    self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
                else:
                    self.number_of_days_temp = 0
                    self.number_of_hours_temp = 0

    @api.model
    def create(self, vals):
        creation = super(MonconHrHolidays, self).create(vals)
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr.holidays', employee.id, self.department_id)
        workflow = None
        if workflow_id:
            workflow = workflow_id
        if not workflow:
            raise exceptions.Warning('There is no workflow defined!')
        creation.workflow_id = workflow
        return creation

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError('Зөвхөн ноорог төлөвтэй үед устгах боломжтой')
        return super(MonconHrHolidays, self).unlink()

    @api.multi
    def action_send(self):
        if self.filtered(lambda holiday: holiday.state != 'draft'):
            raise UserError(_('Leave request must be in Draft state ("To Submit") in order to confirm it.'))
        self.write({'state': 'send'})
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'moncon.hr.holidays.workflow.history', 'history', self, self.employee_id.id)
            if success:
                if sub_success:
                    self.state = 'send'
                else:
                    self.check_sequence = current_sequence
        else:
            raise UserError('Ажлын урсгал үүсээгүй байна')
        return True

    @api.multi
    def action_approve(self):
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'moncon.hr.holidays.workflow.history', 'history', self, self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    # @api.multi
    # def action_refuse(self):
    #     if self.workflow_id:
    #         success = self.env['workflow.config'].reject('hr.holidays.workflow.history', 'history', self, self.env.user.id)
    #         if success:
    #             self.state = 'refuse'
    #
    #     for holiday in self:
    #         holiday.write({'state': 'refuse'})
    #
    #     return True

    @api.multi
    def _is_workflow_history(self):
        for obj in self:
            is_workflow = False
            user_id = self._uid
            for line in obj.history_lines:
                if user_id == line.user_id.id:
                    is_workflow = True
            obj.is_workflow_history = is_workflow

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['moncon.hr.holidays.workflow.history']
            validators = history_obj.search([('history', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1,
                                            order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.onchange('date_to')
    def _onchange_date_to(self):
        date_from = self.date_from
        date_to = self.date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.type == 'add':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from,
                                                                                                     self.env.user).hour <= 12 and get_day_like_display(date_to,
                                                                                                                                                        self.env.user).hour >= 15 and get_day_like_display(
                        date_to, self.env.user).hour <= 20:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 - 1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.type == 'add':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8
            else:
                if (date_to and date_from) and (date_from <= date_to):
                    self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
                    self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
                else:
                    self.number_of_days_temp = 0
                    self.number_of_hours_temp = 0

    @api.multi
    @api.onchange('date_from')
    def _compute_check_in(self):
        for obj in self:
            if obj.date_from:
                start = datetime.strptime(obj.date_from, '%Y-%m-%d %H:%M:%S').date()
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(obj.employee_id, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.check_out = attendance.check_out

    @api.multi
    def register(self):
        for obj in self:
            if self.type=='add':
                self.employee_id.holiday_day_count += self.number_of_hours_temp
            else:
                self.employee_id.holiday_day_count -= self.number_of_hours_temp
            obj.state = 'accepted'

    @api.multi
    def action_draft(self):
        self.check_sequence = 0
        return self.write({'state': 'draft'})

    def _get_number_of_days(self, date_from, date_to, employee_id):
        if self.employee_id.contract_id.working_hours:
            day_count = 0

            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_count += 1
            return day_count
        else:

            """ Returns a float equals to the timedelta between two dates given as string."""
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            day_count = 0
            if self.env.user.company_id.overtime_holiday:
                days = {'mon': 0, 'tue': 1, 'wed': 2, 'thu': 3, 'fri': 4, 'sat': 5, 'sun': 6}
                delta_day = timedelta(days=1)
                while from_dt <= to_dt:
                    if from_dt.weekday() in (days['sat'], days['sun']):
                        day_count += 1
                    else:
                        holidays = self.env['hr.holidays.configuration'].search([('date', '=', from_dt)])
                        if holidays:
                            day_count += 1
                    from_dt += delta_day

            return time_delta.days - day_count

    # Цагын зөрүүг өгөх функц
    def _get_number_of_hours(self, date_from, date_to):
        total_hours = 0.0
        if self.employee_id.contract_id.working_hours:
            st_date = get_day_by_user_timezone(date_from, self.env.user)
            end_date = get_day_by_user_timezone(date_to, self.env.user)
            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_start_dt = day.replace(hour=0, minute=0, second=0)
                day_end_dt = day.replace(hour=23, minute=59, second=59)
                if day.date() == st_date.date():
                    start_date = st_date
                else:
                    start_date = day_start_dt
                if day.date() == end_date.date():
                    finish = end_date
                else:
                    finish = day_end_dt
                resource_calendar = self.employee_id.contract_id.working_hours
                hours = resource_calendar.get_working_hours_of_date(start_dt=start_date, end_dt=finish,
                                                                    compute_leaves=True, resource_id=None,
                                                                    default_interval=None)
                total_hours += hours
        else:
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            total_hours = float(time_delta.seconds) / 3600
        return total_hours


class ExpenseWorkflowHistory(models.Model):
    _name = 'moncon.hr.holidays.workflow.history'
    _order = 'history, sent_date'

    STATE_SELECTION = [('waiting', 'Waiting'),
                       ('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('return', 'Return'),
                       ('rejected', 'Rejected')]
    history = fields.Many2one('moncon.hr.holidays', 'Expense', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_moncon_holidays_workflow_history_ref', 'history', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    holiday_day_count = fields.Float(u'Хуримтлагдсан цаг')
    holiday_day_display =  fields.Float(u'Хуримтлагдсан цаг', compute='compute_holiday_day_display')

    @api.multi
    def compute_holiday_day_display(self):
        self.holiday_day_display = self.holiday_day_count


    @api.multi
    def stat_button_hr_holidays(self):
        #  Тухайн ажилтаны хуримтлагдсан цаг

        for line in self:
            res = self.env['moncon.hr.holidays'].search([('employee_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': u'Хуримтлагдсан цаг',
                'res_model': 'moncon.hr.holidays',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }
