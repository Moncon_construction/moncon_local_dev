# -*- coding: utf-8 -*-
import hashlib
import hmac

from odoo import api, fields, models, _

from odoo.http import request


class WorkflowConfig(models.Model):
    _inherit = 'workflow.config'

    def get_email_body(self, model_object, url, obj=None):
        if model_object._name == 'hr.holidays' and model_object.holiday_status_id.is_nohon_amrakh == False:
            base_link = self.env['ir.config_parameter'].get_param('web.base.url')
            params = {
                'model': model_object._name,
                'res_id': model_object.id
            }
            url = "%s/web#id=%s&view_type=form&model=%s" % (base_link, model_object.id, model_object._name)
            valid_token = request.env['mail.thread']._generate_notification_token(base_link, params)
            model_object.write({
                "validate_token": valid_token,
                "is_token_valid": True
            })
            return u"""
                    <p>Сайн байна уу, </br></p><p>Танд батлах %s ирсэн байна.</br></p>
                    <p>Ажилтан %s нь "%s" шалтгаанаар "%s" төрлийн чөлөө хүссэн байна. </p>
                    <p>Дэлгэрэнгүй мэдээллийг харахыг хүсвэл "Харах" товчийг дарна уу.</br></p>
                    <p><a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Харах</a> |
                    <p>Od ERP Автомат Имэйл </p>
                """ % (obj if obj else model_object.name, model_object.employee_id.name, model_object.name, model_object.holiday_status_id.name, url)
        elif model_object._name == 'hr.holidays' and model_object.holiday_status_id.is_nohon_amrakh == True:
                base_link = self.env['ir.config_parameter'].get_param('web.base.url')
                params = {
                    'model': model_object._name,
                    'res_id': model_object.id
                }
                # Тухайн удамшуулсан сүүлийн харагдацыг дуудаж байна.
                base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                action_id = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_hr_holidays', 'act_hr_employee_holiday_mooncon_nohon_amrah_action')[1],
                url = "%s/web#id=%s&view_type=form&model=%s&action=%s" % (base_url,  model_object.id, model_object._name, action_id[0])
                valid_token = request.env['mail.thread']._generate_notification_token(base_link, params)
                model_object.write({
                    "validate_token": valid_token,
                    "is_token_valid": True
                })
                if model_object.nohon_holidays_type == 'nohon_add':
                    nohon_name = u'Нөхөн амрах хоног хуримтлуулах'
                else:
                    nohon_name = u'Нөхөн амрах хоног биеэр эдлэх'
                return u"""
                        <p>Сайн байна уу, </br></p><p>Танд батлах %s ирсэн байна.</br></p>
                        <p>Ажилтан %s нь "%s" шалтгаанаар "%s" төрлийн чөлөө хүссэн байна. </p>
                        <p>Дэлгэрэнгүй мэдээллийг харахыг хүсвэл "Харах" товчийг дарна уу.</br></p>
                        <p><a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Харах</a> |
                        <p>Od ERP Автомат Имэйл </p>
                    """ % (obj if obj else model_object.name, model_object.employee_id.name, model_object.name, nohon_name, url)


        return super(WorkflowConfig, self).get_email_body(model_object, url, obj)

    def get_email_subject(self, model_object):
        if model_object._name == 'hr.holidays':
            return _('Leave: %s' % model_object.display_name)
        return super(WorkflowConfig, self).get_email_subject(model_object)