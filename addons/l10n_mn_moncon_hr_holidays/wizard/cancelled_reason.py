# -*- coding: utf-8 -*-

from odoo import _, api, fields, models

class CancelledReason(models.TransientModel):
    _name = 'cancelled.reason'
    _description = u'Цуцлах тайлбар'

    reason = fields.Char('Тайлбар', required=True)

    def cancelled_reason(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        for line in self.env['moncon.hr.holidays'].browse(active_ids):
            if line.workflow_id:
                success = line.env['workflow.config'].reject('moncon.hr.holidays.workflow.history', 'history', line, line.env.user.id)
                if success:
                    line.cancelled_reason = self.reason
                    line.write({'state': 'refuse'})

            line.write({'state': 'refuse'})