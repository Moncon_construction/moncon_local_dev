# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Moncon Human Resource Contract",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr_payroll',
        'l10n_mn_moncon_kpi',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         Moncon HR contract
    """,
    'data': [
        'views/hr_contract_views.xml',
        'views/hr_moncon_payslip.xml',
        'wizard/hr_contract_info_change_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
