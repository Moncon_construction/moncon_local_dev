# -*- coding: utf-8 -*-

{
    'name': "NOMADS - Mongolian HR Roster",
    'version': "1.0",
    'depends': [
        'l10n_mn_hr_roster', 
        'l10n_mn_moncon_hr_attendance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'description': """ Түүхий ирц боловсруулах ажлыг roster суусан үед ажилладаг болговы """,
    'data': [
    ],
}
