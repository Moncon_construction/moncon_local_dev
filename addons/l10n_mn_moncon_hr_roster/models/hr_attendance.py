# -*- coding: utf-8 -*-

from datetime import datetime

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

from lxml import etree
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import AccessError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        # Ирц давхардаж үүсгэх шалгалтуудыг авав.
        return
    
    