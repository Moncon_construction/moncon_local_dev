# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *

class HourBalance(models.Model):
    _inherit = "hour.balance"

    hr_employee_id = fields.Many2one('hr.employee', 'ХН ажилтан')
    department_hr_tag_ids = fields.Many2one('department.hr.tag',  string="Tags")



    # def get_linked_objects(self, object, employee, st_date, end_date):
    #     if object != 'hr.holidays':
    #         return self.env['object'].sudo().search([])
    #
    #     domain = [('employee_id', '=', employee.id), ('type', '=', 'remove'), ('state', '=', 'accepted')]
    #     if 'balance_type' in self._context.keys():
    #         domain.append(('holiday_status_id.balance_type', '=', self._context['balance_type']))
    #         domain.append(('holiday_status_id.is_nohon_amrakh', '=', self._context['is_nohon_amrakh']))
    #     domain.extend(get_duplicated_day_domain('date_from', 'date_to', st_date, end_date, self.env.user))
    #
    #     return self.env[object].sudo().search(domain)

    def get_available_employees(self):
        employee_obj = self.env['hr.employee'].sudo()
        employees = []

        for obj in self:
            # 'Урьдчилгаа цалин' төрөлтэй цагийн баланс үед 'Жирэмсэн' болон 'Ажлаас гарсан' төлөвтэй ажилчдыг НЭМЭХГҮЙ байх
            ignored_states = ['resigned', 'maternity'] if obj.salary_type == 'advance_salary' else ['resigned']
            ignored_statuses = self.env['hr.employee.status'].sudo().search([('type', 'in', ignored_states)])

            # Update of balance sheet information
            domain = [('active', '=', True), ('company_id', '=', obj.company_id.id)]
            if ignored_statuses and len(ignored_statuses) > 0:
                domain.append(('state_id', 'not in', ignored_statuses.sudo().ids))
            if obj.department_id:
                domain.append(('department_id', 'child_of', [obj.department_id.id]))
            if obj.hr_employee_id:
                domain.append(('hr_employee_id', '=', obj.hr_employee_id.id))
            if obj.department_hr_tag_ids:
                 domain.append(('department_hr_tag_ids', '=', obj.department_hr_tag_ids.id))
            employees.extend(employee_obj.search(domain))
        return employees

    # def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
    #     self.ensure_one()
    #
    #     date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
    #     period_date_from, period_date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date, is_balance=False)
    #     if not (date_from and date_to) or not (period_date_from and period_date_to):
    #         return {}
    #
    #     reg_hours = self.calc_reg_day(period_date_from, period_date_to, employee_id, contract_id)
    #     balance_reg_hours = self.calc_reg_day(date_from, date_to, employee_id, contract_id, is_balance=True)
    #
    #     if self.can_calc_reg_day(employee_id, contract_id):
    #         reg_day = reg_hours['working_day']
    #         reg_hour = reg_hours['working_hour']
    #         balance_reg_day = balance_reg_hours['working_day']
    #         balance_reg_hour = balance_reg_hours['working_hour']
    #     else:
    #         reg_day, reg_hour, balance_reg_day, balance_reg_hour = 0, 0, 0, 0
    #
    #     if date_from and date_to:
    #         attendance_hour, total_attendance_days = self.get_total_attendance_hours_days(employee_id, date_from,
    #                                                                                       date_to)  # Нийт ирцийн цаг/өдөр - Амралт нтр тооцохгүй. Ирсэн огноо болон гарсан огнооноос цайны цагийг хассан хугацаа байна
    #
    #         paid_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                        contract_id, date_from, date_to)
    #         unpaid_holiday = self.with_context({'balance_type': 'unpaid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                            contract_id, date_from, date_to)
    #         annual_leave = self.with_context({'balance_type': 'annual_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                                contract_id, date_from,
    #                                                                                                                                date_to)
    #         sick_leave_holiday = self.with_context({'balance_type': 'sick_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays',
    #                                                                                                                                    employee_id, contract_id,
    #                                                                                                                                    date_from, date_to)
    #         rehabilitated_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': True}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                                contract_id, date_from,
    #                                                                                                                                date_to)
    #     else:
    #         attendance_hour, total_attendance_days = 0, 0
    #         paid_holiday, unpaid_holiday, annual_leave, sick_leave_holiday, rehabilitated_holiday = 0, 0, 0, 0
    #
    #     values = {
    #         'origin': 'by_attendance',
    #         'employee_id': employee_id.id,
    #         'contract_id': contract_id.sudo().id,
    #         'employee_reg_number': employee_id.ssnid,
    #         'employee_last_name': employee_id.last_name,
    #         'department_id': self.department_id.id,
    #         'balance_id': self.id,
    #         'salary_type': self.salary_type,
    #         'reg_day': reg_day if reg_day > 0 else 0,
    #         'reg_hour': reg_hour if reg_hour > 0 else 0,
    #         'balance_reg_day': balance_reg_day if balance_reg_day > 0 else 0,
    #         'balance_reg_hour': balance_reg_hour if balance_reg_hour > 0 else 0,
    #         'attendance_hour': attendance_hour,
    #         'total_attendance_days': total_attendance_days,
    #         'paid_holiday': paid_holiday,
    #         'unpaid_holiday': unpaid_holiday,
    #         'annual_leave': annual_leave,
    #         'sick_leave': sick_leave_holiday,
    #         'rehabilitated_holiday': rehabilitated_holiday,
    #     }
    #     return values

class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"

    balance_id = fields.Many2one('hour.balance', string='Hour Balance', required=True)
    rehabilitated_add_holiday = fields.Float(string='Нөхөж амрах цаг', digits=(4, 2))
    rehabilitated_holiday = fields.Float(string='Нөхөж амраасан цаг', digits=(4, 2))
    attendance_repair_count = fields.Integer(compute="_attendance_repair_count")
    hr_holidays_count = fields.Integer(compute='_hr_holidays_count')
    hr_holidays_nohon_count = fields.Integer(compute='_hr_holidays_nohon_count')
    hr_holidays_add_nohon_count = fields.Integer(compute='_hr_holidays_add_nohon_count')

    balance_date_from = fields.Date(related='balance_id.balance_date_from')
    balance_date_to = fields.Date(related='balance_id.balance_date_to')
    total_worked_hour_balance = fields.Float(string='Хянах цаг', readonly=True, compute='compute_total_worked_hour_balance', digits=(4, 2))
    total_worked_hour = fields.Float(string='Total worked /hours/', readonly=True, compute='compute_total_worked_hour', digits=(4, 2))
    project_id = fields.Many2one(related='employee_id.project_id', store=True, readonly=False, string='Төсөл')

    check_attendance_month_field = fields.Float('Ирц', compute='compute_check_attendance_month_field')
    missed_hour_moncon = fields.Float(string='Таслалт/цаг/', compute='compute_missed_hour_moncon', store=True, readonly=False, digits=(4, 2))

    @api.depends('balance_reg_hour', 'total_worked_hour', 'paid_holiday', 'unpaid_holiday', 'annual_leave', 'sick_leave', 'out_working_hour',
                 'business_trip_hour', 'total_training_hours', 'delay_hour', 'unworked_leaved_hour')

    def compute_missed_hour_moncon(self):
        # ТАСЛАЛТ = БАЛАНСЫН АЖИЛЛАВАЛ ЗОХИХ /ЦАГ/ - ТОМИЛОЛТ – ГАДУУР АЖИЛ – СУРГАЛТ /ЦАГ/ - ЦАЛИНТАЙ ЧӨЛӨӨ /ЦАГ/ - ЭЭЛЖИЙН АМРАЛТ  /ЦАГ/ - ӨВЧНИЙ ЧӨЛӨӨ  /ЦАГ/ - ЦАЛИНГҮЙ ЧӨЛӨӨ  /ЦАГ/ - НИЙТ АЖИЛЛАСАН ЦАГ - Сул зогсолт - Ажиллаагүй болон ажлаас гарсан өдөр
        for obj in self:
            if obj.company_id.hr_contract_number == 1:
                holiday_hours = obj.paid_holiday + obj.unpaid_holiday + obj.annual_leave + obj.sick_leave + obj.rehabilitated_holiday
                missed_hour = obj.reg_hour - obj.worked_hour - obj.business_trip_hour - obj.delay_hour - holiday_hours - obj.lag_hour
                obj.missed_hour_moncon = max(missed_hour, 0)


    def compute_check_attendance_month_field(self):
        attendanc_repair_ids = self.env['hr.attendance'].search(
            [('employee_id', '=', self.employee_id.id), ('check_in', '>=', self.balance_id.balance_date_from),
             ('check_in', '<=', self.balance_id.balance_date_to)])
        count = len(attendanc_repair_ids)
        self.check_attendance_month_field = count

    def check_attendance_month(self):
        attendanc_ids = self.env['hr.attendance'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('check_in', '>=', self.balance_id.balance_date_from),
             ('check_in', '<=', self.balance_id.balance_date_to)])

        form_id = self.env.ref("l10n_mn_hr_attendance.hr_attendance_view_form")
        tree_id = self.env.ref("l10n_mn_hr_attendance.view_attendance_tree_inherit_attendance")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Ирц',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.attendance',
            'domain': [('id', 'in', attendanc_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def compute_total_worked_hour(self):
        for obj in self:
            obj.total_worked_hour = obj.worked_hour + obj.paid_holiday + obj.business_trip_hour + obj.rehabilitated_holiday + obj.lag_hour

    def compute_total_worked_hour_balance(self):
        for obj in self:
            obj.total_worked_hour_balance = obj.worked_hour + obj.paid_holiday + obj.lag_hour + obj.business_trip_hour + obj.rehabilitated_holiday + obj.delay_hour

    def _attendance_repair_count(self):
        attendanc_repair_ids = self.env['hr.attendance.repair'].search([('employee_id', '=', self.employee_id.id), ('date_of_attendance', '>=', self.balance_id.balance_date_from), ('date_of_attendance', '<=', self.balance_id.balance_date_to)])
        count = len(attendanc_repair_ids)
        self.attendance_repair_count = count

    def _hr_holidays_count(self):
        hr_holidays_ids = self.env['hr.holidays'].search([('employee_id', '=', self.employee_id.id), ('date_from', '>=', self.balance_id.balance_date_from),
                                                                        ('date_from', '<=', self.balance_id.balance_date_to), ('holiday_status_id.is_nohon_amrakh', '=', False)])
        count = len(hr_holidays_ids)
        self.hr_holidays_count = count

    def _hr_holidays_nohon_count(self):
        hr_holidays_ids = self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),('date_from', '<=', self.balance_id.balance_date_to),
                                                          ('date_from', '>=', self.balance_id.balance_date_from),
                                                          ('type', '=', 'remove')])
        count = len(hr_holidays_ids)
        self.hr_holidays_nohon_count = count

    def _hr_holidays_add_nohon_count(self):

        hr_holidays_ids = self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),('date_from', '<=', self.balance_id.balance_date_to),
                                                          ('date_from', '>=', self.balance_id.balance_date_from),
                                                          ('type', '=', 'add')])
        count = len(hr_holidays_ids)
        self.hr_holidays_add_nohon_count = count

    def check_hr_holidays(self):
        hr_holidays_ids = self.env['hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_from', '>=', self.balance_id.balance_date_from),
             ('date_from', '<=', self.balance_id.balance_date_to),  ('holiday_status_id.is_nohon_amrakh', '=', False)])

        tree_id = self.env.ref("l10n_mn_hr_hour_balance.view_holiday_employee_inherit")
        form_id = self.env.ref("l10n_mn_hr_hour_balance.view_holiday_form_inherit")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Амралт чөлөө',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def check_hr_holidays_nohon_amrakh(self):
        hr_holidays_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),
             ('date_from', '>=', self.balance_id.balance_date_from),
             ('date_from', '<=', self.balance_id.balance_date_to),
             ('type', '=', 'remove')])


        tree_id = self.env.ref("l10n_mn_moncon_hr_holidays.view_moncon_hr_holidays_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Нөхөн амрах хоног биеэр эдлэх',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'moncon.hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def check_hr_holidays_add_nohon_amrakh(self):
        hr_holidays_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),
             ('date_from', '>=', self.balance_id.balance_date_from),
             ('date_from', '<=', self.balance_id.balance_date_to),
             ('type', '=', 'add')])


        tree_id = self.env.ref("l10n_mn_moncon_hr_holidays.view_moncon_hr_holidays_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Нөхөн амрах хоног хуримтлуулах',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'moncon.hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def confirm(self):
        self.sudo().state = 'sent'

    def check_attendance(self):
        attendanc_repair_ids = self.env['hr.attendance.repair'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_of_attendance', '>=', self.balance_id.balance_date_from),
             ('date_of_attendance', '<=', self.balance_id.balance_date_to)])

        tree_id = self.env.ref("l10n_mn_hr_attendance_repair.view_attendance_repair_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_attendance.hr_attendance_repair_form_moncon_inherit")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Ирц нөхөн засвар',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.attendance.repair',
            'domain': [('id', 'in', attendanc_repair_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }


    def update_line(self):
        if self.company_id.hr_contract_number == 1:
            contract = self.employee_id.contract_id
            if contract and contract.active:
                values = self.balance_id.get_balance_line_values(self.employee_id, contract)
                if self.balance_id.can_insert_balance_line(self.employee_id, contract, self.balance_id.get_active_hours(values)):
                    self.sudo().write(values)

