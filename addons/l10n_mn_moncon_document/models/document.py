# -*- coding: utf-8 -*-.

from odoo import api, fields, models  # @UnresolvedImport

import logging

_logger = logging.getLogger('odoo')


class ReceivedDocument(models.Model):
    _inherit = 'received.document'

    def _default_documnet_type(self):
        type = self.env['document.type'].search([('type', '=', 'in_doc')], limit=1)
        return type.id

    document_date = fields.Date('Баримтын огноо')
    document_type = fields.Many2one('document.type', 'Document Type', states={'draft': [('readonly', False)]}, track_visibility='onchange', default=_default_documnet_type)


class SentDocument(models.Model):
    _inherit = 'sent.document'

    def _default_documnet_type(self):
        type = self.env['document.type'].search([('type', '=', 'out_doc')], limit=1)
        return type.id

    registration_number = fields.Char('Бүртгэлийн дугаар')
    document_date = fields.Date('Баримтын огноо')
    document_type = fields.Many2one('document.type', 'Document Type', states={'draft': [('readonly', False)]}, track_visibility='onchange', default=_default_documnet_type)

    @api.model
    def create(self, vals):
        vals['registration_number'] = self.env['ir.sequence'].next_by_code('sent.document')
        return super(SentDocument, self).create(vals)


class DocumentType(models.Model):
    _inherit = 'document.type'

    type = fields.Selection([('in_doc', u'Ирэх баримт'),
                             ('out_doc', u'Явах баримт')])
