# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Moncon Calendar",
    'version': '10.0.1.0',
    'depends': [
    'l10n_mn_discuss', 'web_calendar'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'description': """
         Moncon Calendar
    """,
    'data': [
        'views/calendar_views.xml',
        'views/web_calendar_templates.xml'
    ],
    'installable': True,
    'auto_install': False
}